import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { routerTransition } from '../router.animations';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  animations: [routerTransition()],
  encapsulation : ViewEncapsulation.None
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  constructor(public router: Router, public formBuilder: FormBuilder) { 
    this.loginForm = formBuilder.group({
      username: ['', Validators.compose([Validators.required, Validators.pattern('[a-zA-Z]*'), Validators.minLength(4), Validators.maxLength(30)])],
      password: ['', Validators.compose([Validators.required, Validators.minLength(4)])]
    });
  }

  ngOnInit() {
  }
  
  onLoggedin(userData) {
    console.log(userData);
    this.router.navigate(['/home']);
    localStorage.setItem('isLoggedin', 'true');
  }
}
