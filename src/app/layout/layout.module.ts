import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { LayoutRoutingModule } from "./layout-routing.module";
import { LayoutComponent } from "./layout.component";
import { SidebarComponent } from "../components/sidebar/sidebar.component";
import { HeaderComponent } from "../components/header/header.component";
import { NgbDropdownModule, NgbModalModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
    imports : [
        CommonModule,
        LayoutRoutingModule,
        NgbModalModule,
        NgbDropdownModule.forRoot()
    ],
    declarations : [
        LayoutComponent, SidebarComponent, HeaderComponent
    ]
})

export class LayoutModule {};